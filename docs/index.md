# 활성화 함수과 그 중요성 <sup>[1](#footnote_1)</sup>

> <font size="3">활성화 함수는 어떻게 사용되는지, 왜 중요한지 알아본다.</font>

## 목차

1. [개요](./activation-functions.md#intro)
1. [활성화 함수란?](./activation-functions.md#sec_02)
1. [활성화 함수의 종류](./activation-functions.md#sec_03)
1. [활성화 함수의 선택](./activation-functions.md#sec_04)
1. [활성화 함수의 응용](./activation-functions.md#sec_05)
1. [요약](./activation-functions.md#summary)

<a name="footnote_1">1</a>: [DL Tutorial 3 — Activation Functions and Their Importance](https://medium.com/ai-in-plain-english/dl-tutorial-3-activation-functions-and-their-importance-a5343a81a15a?sk=322da625a5cd2c872e98e584c8534bc7)를 편역하였다.
